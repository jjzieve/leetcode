class Solution:
	# @param num, a list of integers
	# @return an integer
	def majorityElement(self, num):
		majority_element_cutoff = int(len(num)/2)
		counts = {}
		for n in num:
			if n in counts:
				counts[n] += 1
				if counts[n] > majority_element_cutoff:
					return n
			else:
				counts[n] = 1
				if counts[n] > majority_element_cutoff:
					return n
		print majority_element_cutoff
		print counts

solution = Solution()
print solution.majorityElement([1])