#basically, base-26 it 

class Solution:
	# @return a string
	def convertToTitle(self, num):
		num -= 1
		alphabet = [chr(x) for x in range(ord("A"),ord("Z")+1)]
		if(num >= 26):
			return self.convertToTitle((num/26)) + alphabet[(num % 26)]
		else:
			return alphabet[(num % 26)]

	def titleToNumber(self,s):
		num = 0
		for i,char in enumerate(reversed(s)):
			num += pow(26,i) * ((ord(char) - ord("A"))+1)
		return num

sol = Solution()
print sol.titleToNumber("A")